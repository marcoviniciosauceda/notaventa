/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaventa;

/**
 *
 * @author marco
 */
public abstract class Producto {
    protected int id;
    protected String nombre;
    protected int unidad;
    protected float precioUnitario;
    
    public Producto(){
        this.id = 0;
        this.nombre = "";
        this.unidad = 0;
        this.precioUnitario = 0.0f;
    }

    public Producto(int id, String nombre, int unidad, float precioUnitario) {
        this.id = id;
        this.nombre = nombre;
        this.unidad = unidad;
        this.precioUnitario = precioUnitario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getUnidad() {
        return unidad;
    }

    public void setUnidad(int unidad) {
        this.unidad = unidad;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
    
public abstract float calcularPrecio();
    
    
}
