/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaventa;

/**
 *
 * @author marco
 */
public class NoPerecedero extends Producto{
    private String lote;

    public NoPerecedero(String lote) {
        this.lote = lote;
    }

    public NoPerecedero(String lote, int id, int unidad, float precio, String nombre) {
        super(id, unidad, precio, nombre);
        this.lote = lote;
    }

    public NoPerecedero() {
       this.lote = "";
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    
    @Override
    public float calcularPrecio() {
        return this.precio*1.5f;
    }
    
    
}

